package com.company;



    public class LinearCalculation {

        public static double findCarsDistance(double car1Speed, double car2Speed, double initialDistance, double time) {
            double totalSpeed = car1Speed + car2Speed;
            double generalWay = time * totalSpeed;
            double distance = initialDistance + generalWay;
            return distance;
        }

        public static void main(String[] args) {
            double car1Speed = 60.0; // Speed of car 1 in km/h
            double car2Speed = 70.0; // Speed of car 2 in km/h
            double initialDistance = 100.0; // Initial distance between cars in km
            double time = 2.5; // Time in hours

            double finalDistance = findCarsDistance(car1Speed, car2Speed, initialDistance, time);
            System.out.println("The distance between the cars after " + time + " hours is: " + finalDistance + " km");
        }
    

}
